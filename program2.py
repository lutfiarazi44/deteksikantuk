from scipy.spatial import distance
from imutils import face_utils
import imutils
import dlib
import cv2
import pymysql
import RPi.GPIO as GPIO
import time

def eye_aspect_ratio(eye):
    A = distance.euclidean(eye[1], eye[5])
    B = distance.euclidean(eye[2], eye[4])
    C = distance.euclidean(eye[0], eye[3])
    ear = (A + B) / (2.0 * C)
    return ear

def mouth_aspect_ratio (mouth) :
    A = distance.euclidean(mouth[14], mouth[18])
    B = distance.euclidean(mouth[15], mouth[17])
    C = distance.euclidean(mouth[13], mouth[16])

    MAR = (A + B) / (2.0 * C)
    return MAR
def convertToBinaryData(filename):
    with open(filename, 'rb') as file:
        binaryData = file.read()
    return binaryData

def insertBLOB(photo, data):
    print("Inserting BLOB into python_employee table")
    try:
        db = pymysql.connect("192.168.0.101","root","","database" )

        cursor = db.cursor()
        sql_insert_blob_query = """ INSERT INTO db_kamera (photo,data) VALUES (%s,%s)"""
        
        empPicture = convertToBinaryData(photo)
    
        insert_blob_tuple = (empPicture,data)
        result = cursor.execute(sql_insert_blob_query, insert_blob_tuple)
        db.commit()
        print("sukses input data", result)
        
    except :
        db.rollback()

def relayON () :
    limit = 2
    while limit >0 :
        GPIO.setmode(GPIO.BCM) 
        RELAIS_1_GPIO = 17
        GPIO.setup(RELAIS_1_GPIO, GPIO.OUT) # GPIO Assign mode
    #GPIO.output(RELAIS_1_GPIO, GPIO.LOW) # out
        GPIO.output(RELAIS_1_GPIO, GPIO.HIGH) # on
        time.sleep(0.4)
        GPIO.output(RELAIS_1_GPIO, GPIO.LOW)
    
        limit = limit -1
        GPIO.cleanup()

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("/home/pi/Desktop/ta/shape_predictor_68_face_landmarks.dat")

(lS, lE) = face_utils.FACIAL_LANDMARKS_68_IDXS["left_eye"]
(rS, rE) = face_utils.FACIAL_LANDMARKS_68_IDXS["right_eye"]
(mS, mE) = face_utils.FACIAL_LANDMARKS_68_IDXS["mouth"]

camera = cv2.VideoCapture (0)
tambah = 0
tambahMAR = 0 
mar_list = []


while True :
    ret, frame = camera.read()
    frame= imutils.resize(frame, width = 300)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    face = detector (gray,0)

    for face in face :
        shape = predictor(gray,face)
        shape = face_utils.shape_to_np(shape)

        mataKiri = shape [lS:lE]
        mataKanan = shape [rS :rE]
        mulut = shape [mS : mE]
        earKiri = eye_aspect_ratio(mataKiri)
        earKanan = eye_aspect_ratio (mataKanan)
        ear = (earKiri + earKanan)/ 2.0
        MAR = mouth_aspect_ratio (mulut)
        

        if ear < 0.2 :
            tambah +=1 
            if tambah >= 10 :
                print ("Mengantuk")
                cv2.putText(frame, "SEDANG MENGANTUK", (50, 30),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
                #setup relay
                relayON ()
                
        
        elif MAR > 0.8 :
            tambah += 1
            
            if tambah >= 10 :
                print("Mengantuk 1")
                cv2.putText(frame, "SEDANG MENGANTUK", (50, 30),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
                relayON ()
                cv2.imwrite('image.jpg',frame)
                #insertBLOB ('/home/pi/Desktop/ta/image.jpg',1)
        else:

            tambah = 0 
        
        #print (MAR)

    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break  
cv2.destroyAllWindows()
camera.stop()

