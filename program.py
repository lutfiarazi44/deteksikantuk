from scipy.spatial import distance
from imutils import face_utils
import imutils
import dlib
import cv2
import pymysql
import RPi.GPIO as GPIO
import time
import os
import base64
import requests as req
from urllib.request import urlopen

def eye_aspect_ratio(eye):
    A = distance.euclidean(eye[1], eye[5])
    B = distance.euclidean(eye[2], eye[4])
    C = distance.euclidean(eye[0], eye[3])
    eye = (A + B) / (2.0 * C)
    return eye

def mouth_aspect_ratio (mouth) :
    A = distance.euclidean(mouth[14], mouth[18])
    B = distance.euclidean(mouth[15], mouth[17])
    C = distance.euclidean(mouth[13], mouth[16])

    MAR = (A + B) / (2.0 * C)
    return MAR

def kirim ():
    with open('/home/pi/Desktop/ta/resize3.jpg', 'rb') as binary_file:
        binary_file_data = binary_file.read()
        base64_encoded_data = base64.b64encode(binary_file_data)
        base64_message = base64_encoded_data.decode('utf-8')
    #gambar
    datatabel3 = {'kamera': base64_message, 'series':1 }
    resp = req.get('http://ta202101008.elsinta-itera.com/belajar/index.php/Simpan/gambar',params=datatabel3)
    #series alat
    url = resp.url
    urlopen(url)
    print(url)

def relayON () :
    GPIO.setmode(GPIO.BCM) 
    RELAIS_1_GPIO = 17
    GPIO.setup(RELAIS_1_GPIO, GPIO.OUT) # GPIO Assign mode
    GPIO.output(RELAIS_1_GPIO, GPIO.LOW) # out
    GPIO.output(RELAIS_1_GPIO, GPIO.HIGH) # on
    time.sleep(5)
    GPIO.output(RELAIS_1_GPIO, GPIO.LOW)
    GPIO.cleanup()

def cek_koneksi () :
    hostname = "google.com"
    response = os.system("ping -c 1 " + hostname)
    if response == 0:
        pingstatus = "Network Active"
        kirim ()
        print ("Ada koneksi")
        time.sleep(5)
    else:
        pingstatus = "Network Error"
        print ("Tidak ada internet")

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("/home/pi/Desktop/ta/shape_predictor_68_face_landmarks.dat")

(lS, lE) = face_utils.FACIAL_LANDMARKS_68_IDXS["left_eye"]
(rS, rE) = face_utils.FACIAL_LANDMARKS_68_IDXS["right_eye"]
(mS, mE) = face_utils.FACIAL_LANDMARKS_68_IDXS["mouth"]

camera = cv2.VideoCapture (0)
tambah = 0

mar_list = []


while True :
    ret, frame = camera.read()
    frame= imutils.resize(frame, width = 300)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    face = detector (gray,0)

    for face in face :
        shape = predictor(gray,face)
        shape = face_utils.shape_to_np(shape)

        mataKiri = shape [lS:lE]
        mataKanan = shape [rS :rE]
        mulut = shape [mS : mE]
        earKiri = eye_aspect_ratio(mataKiri)
        earKanan = eye_aspect_ratio (mataKanan)
        eye = (earKiri + earKanan)/ 2.0
        MAR = mouth_aspect_ratio (mulut)
        
        #deteksi mata dan bibir
        if eye < 0.19 :
            tambah +=1
            if tambah >= 10 and tambah <= 10.8 :
                print ("Mengantuk")
                cv2.putText(frame, "SEDANG MENGANTUK", (50, 30),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
                relayON ()
                
                h,w = frame.shape[:2]
                new_h, new_w = int(h/4),int(w/4)
                frame = cv2.resize(frame, (new_w,new_h))
                cv2.imwrite('resize3.jpg',frame)
                cek_koneksi ()
                
        elif MAR > 0.7 :
            tambah += 1
            #tambahMar =+1
            if tambah >= 10 and tambah <= 10.8 :
                print("Mengantuk 1")
                cv2.putText(frame, "SEDANG MENGANTUK", (50, 30),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
                relayON ()
                
                h,w = frame.shape[:2]
                new_h, new_w = int(h/4),int(w/4)
                frame = cv2.resize(frame, (new_w,new_h))
                cv2.imwrite('resize3.jpg',frame)
                cek_koneksi ()
                
        else:

            tambah = 0 
        

    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break  
cv2.destroyAllWindows()
camera.stop()
